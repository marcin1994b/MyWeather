package com.marcinbaranski.myweather.myweather.model.services

import com.google.android.gms.location.places.Place
import com.marcinbaranski.myweather.myweather.model.entity.imageData.ImageData
import com.marcinbaranski.myweather.myweather.model.entity.weatherData.WeatherResponse
import com.marcinbaranski.myweather.myweather.model.providers.LocationProvider
import com.marcinbaranski.myweather.myweather.model.providers.NetworkProvider
import com.marcinbaranski.myweather.myweather.model.providers.WeatherDataProvider
import com.marcinbaranski.myweather.myweather.model.providers.WeatherImageProvider

class DataDownloadingServices(private val locationProvider: LocationProvider,
                              private val networkProvider: NetworkProvider)
    : LocationProvider.OnLocationProviderResult, WeatherDataProvider.WeatherDataProvider,
        WeatherImageProvider.WeatherImageProvider {

    val NO_ERR_CODE = 0
    val ERR_LOCATION = 1
    val ERR_NETWORK = 2
    val ERR_CODE = 3

    private val TODAY_TAG = 1
    private val TOMORROW_TAG = 2

    //private var city = ""
    //private var units = ""

    private var weatherResponse: WeatherResponse = WeatherResponse()
    //private var todayWeatherImg: ImageData? = null
    //private var tomorrowWeatherImg: ImageData? = null

    lateinit var receiver: onDataDownloadingServiceResult

    interface onDataDownloadingServiceResult {
        fun onDatDownloadingServiceResult(isSuccess: Boolean, data: WeatherResponse, error: Int)
    }

    fun startDownloading(place: Place?, units: String, unitsType: Int) {
        //this.units = units
        this.weatherResponse.units = units
        this.weatherResponse.unitsType = unitsType
        if(place != null){
            //this.city = place.name.toString()
            getWeatherDataForLocation(place)
        }else{
            //this.city = ""
            getWeatherDataForCurrentLocation()
        }
    }

    fun onResume() = locationProvider.onResume()

    fun onPause() = locationProvider.onPause()

    fun onStop() = locationProvider.onStop()

    private fun getWeatherDataForLocation(place: Place) =
            with(this.weatherResponse){
                this.latitude = place.latLng.latitude
                this.longitude = place.latLng.longitude
                this.cityName = place.name.toString()
                downloadWeatherData()
            }


    private fun getWeatherDataForCurrentLocation() =
            when(networkProvider.isNetworkAvailable()){
                true -> getCurrentLocation()
                false -> receiver.onDatDownloadingServiceResult(false, weatherResponse, ERR_NETWORK)
            }

    private fun getCurrentLocation(){
        locationProvider.receiver = this
        locationProvider.onStart()
        locationProvider.getLocation()
        locationProvider.onStop()
    }

    override fun onLocationApiHandlerResult(isSuccessful: Boolean, message: String,
                                            data: LocationProvider.LocationHandlerResponse?) =
            when(isSuccessful){
                true -> processLocationData(data)
                false-> receiver.onDatDownloadingServiceResult(false, weatherResponse, ERR_LOCATION)
            }

    private fun processLocationData(data: LocationProvider.LocationHandlerResponse?) =
            when(data){
                null -> receiver.onDatDownloadingServiceResult(false, weatherResponse, ERR_LOCATION)
                else -> {
                    with(this.weatherResponse){
                        this.cityName = data.addressLine
                        this.latitude = data.latitude
                        this.longitude = data.longitude
                    }
                    downloadWeatherData()
                    //city = data.addressLine
                }
            }

    private fun downloadWeatherData() =
            when(networkProvider.isNetworkAvailable()){
                true -> {
                    val weatherDataProvider = WeatherDataProvider(this)
                    weatherDataProvider.downloadForecastData(this.weatherResponse.latitude,
                            this.weatherResponse.longitude, this.weatherResponse.units)
                }
                false -> receiver.onDatDownloadingServiceResult(false, weatherResponse, ERR_NETWORK)
            }

    override fun handleWeatherDataProviderResponse(response: WeatherResponse) =
            with(this.weatherResponse){
                this.currently = response.currently
                this.daily = response.daily
                this.timezone = response.timezone

                when(networkProvider.isNetworkAvailable()){
                    true -> downloadWeatherImages(this.currently.icon, this.daily.data[1].icon)
                    false-> receiver.onDatDownloadingServiceResult(false, weatherResponse, ERR_NETWORK)
                }
            }


    override fun handleWeatherDataProviderError(error: Throwable) =
            receiver.onDatDownloadingServiceResult(false, weatherResponse, ERR_CODE)

    private fun downloadWeatherImages(todayWeatherType: String, tomorrowWeatherType: String){
        downloadImg(todayWeatherType, TODAY_TAG)
        downloadImg(tomorrowWeatherType, TOMORROW_TAG)
    }

    private fun downloadImg(type: String, tag: Int){
        val weatherImageProvider = WeatherImageProvider(this, tag)
        weatherImageProvider.downloadWeatherImages(type)
    }

    override fun handleWeatherImageProviderResponse(response: ImageData, tag: Int) =
            with(this.weatherResponse) {
                when (tag) {
                    TODAY_TAG -> this.todayWeatherImg = response
                    TOMORROW_TAG -> this.tomorrowWeatherImg = response
                }
                ifThatIsAllInformReceiver()
            }


    private fun ifThatIsAllInformReceiver() =
            when(isAllDataDownloaded()){
                true -> receiver.onDatDownloadingServiceResult(true, weatherResponse, NO_ERR_CODE)
                false-> {}
            }

    private fun isAllDataDownloaded(): Boolean =
            (weatherResponse.todayWeatherImg.id.isNotEmpty() && weatherResponse.tomorrowWeatherImg.id.isNotEmpty())

    /*private fun ifThatsAllInformReceiver() =
            weatherResponse?.let{ weather ->
                todayWeatherImg?.let{ today ->
                    tomorrowWeatherImg?.let{ tomorrow ->
                        receiver.onDatDownloadingServiceResult(true, weather,
                                today, tomorrow, city, units, NO_ERR_CODE)
                    }
                }
            }*/

}