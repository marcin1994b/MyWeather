package com.marcinbaranski.myweather.myweather.ui.MainView

import com.google.android.gms.location.places.Place


interface MainActivityContracts {

    interface MainViewPresenterContract {
        fun onViewCreate()
        fun onViewResume()
        fun onSettingsMenuButtonTap()
        fun onSearchMenuButtonTap()
        fun onSearchActivityResultSuccess(place: Place)
        fun onSearchActivityResultCanceled()
        fun onSearchActivityResultError(error: String?)
        fun refreshWeatherDataForFragment()

        fun onNoClickedRatingDialog()
        fun onYesClickedRatingDialog()
        fun onCancelClickedRatingDialog()

    }

    interface MainViewContract{
        fun showToast(msg:String)
        fun setToolbarString(str: String)
        fun startWelcomeActivity()
        fun showRateDialog()
        fun startSearchActivity()
        fun startSettingsActivity()
        fun restartActivity()
        fun finishThisActivity()

        fun fabricLogSearch(query: String)
        fun fabricLogRatingDialogAction(action: String)
    }

}