package com.marcinbaranski.myweather.myweather.ui.DetailWeatherView

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.marcinbaranski.myweather.myweather.R
import com.marcinbaranski.myweather.myweather.ui.DetailWeatherView.DetailFragment.DetailWeatherFragment
import kotlinx.android.synthetic.main.detail_weather_activity.*


class DetailWeatherActivity: AppCompatActivity(), DetailWeatherContracts.View {

    lateinit var presenter: DetailWeatherContracts.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_weather_activity)
        initPresenter()
        setToolbar()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        intent?.let{
            presenter.onViewCreated(it.getIntExtra("position", -1))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean =
            when(item?.itemId){
                android.R.id.home -> {
                    presenter.onHomeButtonTap()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    override fun finishTheActivity() {
        finish()
    }

    override fun startFragment(position: Int){
        val transaction = supportFragmentManager.beginTransaction()
        val fragment = DetailWeatherFragment()
        val bundle = Bundle()
        bundle.putInt("position", position)
        fragment.arguments = bundle
        transaction.add(container.id, fragment, "detailFragment")
        transaction.commit()
    }

    private fun initPresenter(){
        presenter = DetailWeatherViewPresenter(this)
    }

    private fun setToolbar(){
        detail_toolbar.title = "Details"
        setSupportActionBar(detail_toolbar)
    }


}