package com.marcinbaranski.myweather.myweather.model.providers

import android.annotation.SuppressLint
import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import java.util.*



@Suppress("DEPRECATION")
class LocationProvider(private val context: Context)
    : GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    data class LocationHandlerResponse(var longitude: Double,
                                       var latitude: Double,
                                       var addressLine: String)

    interface OnLocationProviderResult{
        fun onLocationApiHandlerResult(isSuccessful: Boolean, message: String,
                                       data: LocationHandlerResponse?)
    }

    private val UPDATE_INTERVAL : Long = 10000
    private val FASTEST_INTERVAL : Long = 5000
    private val DISPLACEMENT : Float = 10.0F

    private val CONNECTION_FAILED = "Connection failed"
    private val COULDNG_GET_ADDRESS = "Couldn't get address"
    private val COULDNT_GET_LOCATION = "Couldn't get location"
    private val DEVICE_NOT_SUPPORTED = "This device is not supported"

    private var mLastLocation: Location? = null
    private var mRequestLocationUpdates: Boolean = false
    private var counter = 0

    private lateinit var mGoogleAPIClient: GoogleApiClient
    private lateinit var mLocationRequest : LocationRequest

    var receiver: LocationProvider.OnLocationProviderResult? = null

    init {
        if(checkPlayServices()){
            buildGoogleApiClient()
            createLocationRequest()
        }
    }

    override fun onConnected(bundle: Bundle?) {
        getLocation()
        if(mRequestLocationUpdates){
            startLocationUpdates()
        }
    }

    override fun onConnectionSuspended(p0: Int) {
        mGoogleAPIClient.connect()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        receiver?.onLocationApiHandlerResult(false, CONNECTION_FAILED, null)

    }

    override fun onLocationChanged(location: Location?) {
        location?.let{
            mLastLocation = it
        }
    }

    fun onStart(){
        mGoogleAPIClient.connect()
    }

    fun onResume(){
        checkPlayServices()
        if(mGoogleAPIClient.isConnected && mRequestLocationUpdates){
            startLocationUpdates()
        }
    }

    fun onStop() {
        if(mGoogleAPIClient.isConnected){
            mGoogleAPIClient.disconnect()
        }
    }

    fun onPause(){
        stopLocationUpdates()
    }

    @SuppressLint("MissingPermission")
    fun getLocation(){
        if(mGoogleAPIClient.isConnected) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleAPIClient, mLocationRequest, this)
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleAPIClient)
        if(mLastLocation != null){
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleAPIClient, this)
            mLastLocation?.let {
                val address: String = getLocationName(it.longitude, it.latitude)
                if (address.isNotEmpty()) {
                    val data = LocationHandlerResponse(it.longitude, it.latitude, address)
                    receiver?.onLocationApiHandlerResult(true, "", data)
                } else {
                    receiver?.onLocationApiHandlerResult(false, COULDNG_GET_ADDRESS, null)
                }
            }
        }else{
            if(counter == 0){
                counter++
            }else {
                receiver?.onLocationApiHandlerResult(false, COULDNT_GET_LOCATION, null)
            }
        }
    }

    private fun getLocationName(longitude: Double, latitude: Double) : String{
        val geocoder = Geocoder(context, Locale.getDefault())
        val addressList : List<Address>? = geocoder.getFromLocation(latitude, longitude, 1)
        addressList?.let{
            if(addressList.isNotEmpty()){
                return addressList[0].locality
            }
        }
        return ""
    }

    private fun buildGoogleApiClient() {
        mGoogleAPIClient = GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build()
    }

    private fun createLocationRequest(){
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = UPDATE_INTERVAL
        mLocationRequest.fastestInterval = FASTEST_INTERVAL
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.smallestDisplacement = DISPLACEMENT
    }

    private fun checkPlayServices() : Boolean {
        val googleAPI = GoogleApiAvailability.getInstance()
        val resultCode = googleAPI.isGooglePlayServicesAvailable(context)
        if(resultCode != ConnectionResult.SUCCESS){
            if(googleAPI.isUserResolvableError(resultCode)){
                receiver?.onLocationApiHandlerResult(false, resultCode.toString(), null)
            }else{
                receiver?.onLocationApiHandlerResult(false, DEVICE_NOT_SUPPORTED, null)
            }
            return false
        }
        return true
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates(){
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleAPIClient, mLocationRequest, this)
    }

    private fun stopLocationUpdates(){
        if(mGoogleAPIClient.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleAPIClient, this)
        }
    }

}