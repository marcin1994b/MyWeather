package com.marcinbaranski.myweather.myweather.model.providers

import com.marcinbaranski.myweather.myweather.model.constants.WeatherTypes
import com.marcinbaranski.myweather.myweather.model.entity.imageData.ImageData
import com.marcinbaranski.myweather.myweather.model.repo.WeatherImagesRepo
import java.util.*


class WeatherImageProvider(private val receiver: WeatherImageProvider, val tag: Int) {

    val SOURCE_LINK = "https://source.unsplash.com/"

    interface WeatherImageProvider{
        fun handleWeatherImageProviderResponse(response: ImageData, tag: Int)
    }

    private val random = Random()

    fun downloadWeatherImages(weatherType: String){
        val repo = WeatherImagesRepo()
        val row = getRowFromRepoImageArray(weatherType, repo)
        val id = repo.list[row][rand(0, repo.list[row].size)]
        val link = SOURCE_LINK + id
        receiver.handleWeatherImageProviderResponse(ImageData(id, link), tag)
    }

    private fun getRowFromRepoImageArray(weatherType: String, repo: WeatherImagesRepo) =
            when(weatherType){
                WeatherTypes.CLEAR_DAY -> repo.CLEAR_DAY
                WeatherTypes.CLEAR_NIGHT -> repo.CLEAR_DAY
                WeatherTypes.RAIN -> repo.RAIN
                WeatherTypes.SNOW -> repo.SNOW
                WeatherTypes.SLEET -> repo.SLEET
                WeatherTypes.WIND -> repo.WIND
                WeatherTypes.FOG -> repo.FOG
                WeatherTypes.PARTLY_CLOUDY_DAY -> repo.PARTLY_CLOUDY
                WeatherTypes.PARTLY_CLOUDY_NIGHT -> repo.PARTLY_CLOUDY
                WeatherTypes.HAIL -> repo.HAIL
                WeatherTypes.THUNDERSTORM -> repo.THUNDERSTORM
                WeatherTypes.TORNADO -> repo.THUNDERSTORM
                WeatherTypes.CLOUDY -> repo.CLOUDY
                else -> 0
            }

    private fun rand(from: Int, to: Int) : Int =
            (random.nextInt(to - from) + from)

}