package com.marcinbaranski.myweather.myweather.ui.DetailWeatherView.DetailFragment

import com.marcinbaranski.myweather.myweather.model.builder.WeatherStringBuilder
import com.marcinbaranski.myweather.myweather.model.entity.imageData.ImageData
import com.marcinbaranski.myweather.myweather.model.entity.weatherData.WeatherData
import com.marcinbaranski.myweather.myweather.model.holders.SettingsHolder
import com.marcinbaranski.myweather.myweather.model.holders.WeatherResponseHolder
import com.marcinbaranski.myweather.myweather.model.providers.TimeDataProvider

class DetailWeatherFragmentPresenter(private val view: DetailWeatherFragmentContracts.View):
        DetailWeatherFragmentContracts.Presenter {

    override fun onResume(position: Int) {
        when(position){
            -1 -> {}
            else -> setTextViewsInView(position)
        }
    }

    private fun setTextViewsInView(position: Int){
        with(WeatherResponseHolder.weatherResponseData.daily.data[position]){
            val stringBuilder = WeatherStringBuilder(WeatherResponseHolder.weatherResponseData.unitsType)
            setWeatherTextView(this, stringBuilder)
            setTemperatureTextView(this, stringBuilder)
            setHumidityTextView(this, stringBuilder)
            setPressureTextView(this, stringBuilder)
            setWindSpeedTextView(this, stringBuilder)
            setCloudCoverTextView(this, stringBuilder)
            setMoonPhaseTextView(this, stringBuilder)
            setSunRiseTextView(this, stringBuilder)
            setSunSetTextView(this, stringBuilder)
            setImageView(WeatherResponseHolder.weatherResponseData.tomorrowWeatherImg)
        }
    }

    private fun setWeatherTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setWeatherDescriptionTextView(stringBuilder.getWeatherDescribeString(data.icon))

    private fun setHumidityTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setHumidityTextView(stringBuilder.getHumidityString(data.humidity))

    private fun setPressureTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setPressureTextView(stringBuilder.getPressureString(data.pressure))

    private fun setWindSpeedTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setWindTextView(stringBuilder.getWindSpeedString(data.windSpeed))

    private fun setCloudCoverTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setCloudCoverTextView(stringBuilder.getCloudCoverString(data.cloudCover))

    private fun setMoonPhaseTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setMoonPhaseTextView(stringBuilder.getMoonPhaseString(data.moonPhase))

    private fun setSunRiseTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) {
        val timeProvider = TimeDataProvider(data.sunriseTime*1000)
        view.setSunriseTextView(stringBuilder.getHourAndMinutesString(timeProvider.getHourOfDay(),
                timeProvider.getMinuteOfHour(), SettingsHolder.timeFormatType))
    }

    private fun setSunSetTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) {
        val timeProvider = TimeDataProvider(data.sunsetTime*1000)
        view.setSunsetTextView(stringBuilder.getHourAndMinutesString(timeProvider.getHourOfDay(),
                timeProvider.getMinuteOfHour(), SettingsHolder.timeFormatType))
    }

    private fun setTemperatureTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setTemperatureTextView(stringBuilder.getTemperatureRangeString(
                    data.temperatureMin, data.temperatureMax))

    private fun setImageView(data: ImageData) = view.setImageView(data.link)


}