package com.marcinbaranski.myweather.myweather.ui.MainView

import com.google.android.gms.location.places.Place
import com.marcinbaranski.myweather.myweather.model.entity.weatherData.WeatherResponse
import com.marcinbaranski.myweather.myweather.model.holders.WeatherResponseHolder
import com.marcinbaranski.myweather.myweather.model.providers.NetworkProvider
import com.marcinbaranski.myweather.myweather.model.providers.PermissionsProvider
import com.marcinbaranski.myweather.myweather.model.providers.SharedPreferencesProvider
import com.marcinbaranski.myweather.myweather.model.providers.TimeDataProvider
import com.marcinbaranski.myweather.myweather.model.services.DataDownloadingServices
import com.marcinbaranski.myweather.myweather.ui.abstractUI.DownloadingBasicActivity.DownloadingBasicViewPresenter


class MainActivityPresenter(private val view: MainActivityContracts.MainViewContract,
                            networkProvider: NetworkProvider,
                            permissionsProvider: PermissionsProvider,
                            private val sharedPreferencesProvider: SharedPreferencesProvider,
                            dataDownloadingServices: DataDownloadingServices)
    :DownloadingBasicViewPresenter(permissionsProvider, networkProvider, sharedPreferencesProvider,
        dataDownloadingServices), MainActivityContracts.MainViewPresenterContract,
        WeatherResponseHolder.ObserverContract{

    val TIME_TO_UPDATE = 30*60000 // 30 min

    private var mPlace: Place? = null

    override fun onStateChange(newValue: Boolean) {}

    override fun update() {
        view.setToolbarString(WeatherResponseHolder.weatherResponseData.cityName)
    }

    override fun getPlace(): Place? = mPlace

    override fun noInternetConnection() {
        WeatherResponseHolder.isDownloading = false
        view.showToast("No Internet connection...")
    }

    override fun onPermissionsDenied()  {
        WeatherResponseHolder.isDownloading = false
        view.showToast("Permissions denied...")
    }

    override fun onDatDownloadingServiceResult(isSuccess: Boolean, data: WeatherResponse, error: Int)  =
            when(isSuccess){
                true -> {
                    WeatherResponseHolder.weatherResponseData = data
                    WeatherResponseHolder.isDownloading = false
                }
                false-> view.showToast("Error")
            }

    override fun refreshWeatherDataForFragment() {
        WeatherResponseHolder.isDownloading = true
        downloadData()
    }

    override fun onSettingsMenuButtonTap() = view.startSettingsActivity()

    override fun onSearchMenuButtonTap() = view.startSearchActivity()

    override fun onSearchActivityResultSuccess(place: Place) {
        view.fabricLogSearch(place.name.toString())
        mPlace = place
        WeatherResponseHolder.isDownloading = true
        downloadData()
    }

    override fun onSearchActivityResultCanceled() {
        view.showToast("Canceled")
    }

    override fun onSearchActivityResultError(error: String?) {
        error?.let{
            view.showToast(error)
        }
    }

    override fun onViewCreate() {
        view.setToolbarString(WeatherResponseHolder.weatherResponseData.cityName)
        view.showRateDialog()
        WeatherResponseHolder.addSubscriber(this)
    }

    override fun onViewResume() {
        if(shouldRefreshWeatherData()){
            refreshWeatherData()
        }
    }

    override fun onCancelClickedRatingDialog() = view.fabricLogRatingDialogAction("Cancel")

    override fun onNoClickedRatingDialog() = view.fabricLogRatingDialogAction("No")

    override fun onYesClickedRatingDialog() = view.fabricLogRatingDialogAction("Yes")

    private fun refreshWeatherData() = downloadData()

    private fun shouldRefreshWeatherData(): Boolean{
        val timeProvider = TimeDataProvider()
        val lastUpdateTime = sharedPreferencesProvider.restoreLastDataUpdateTimeStamp()
        val currentTime = timeProvider.dt.millis
        val subtractionResult = currentTime - lastUpdateTime
        return (subtractionResult>TIME_TO_UPDATE)
    }
}
