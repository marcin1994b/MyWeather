package com.marcinbaranski.myweather.myweather.model.constants


object DateFormatTypes {

    val YEAR_MONTH_DAY = 0
    val DAY_MONTH_YEAR = 1
    val MONTH_DAY_YEAR = 2
    val DEFAULT = DAY_MONTH_YEAR

    val YEAR_MONTH_DAY_DETAILS_STRING = "2018 January 7"
    val DAY_MONTH_YEAR_DETAILS_STRING = "7 January 2018"
    val MONTH_DAY_YEAR_DETAILS_STRING = "January 7, 2018"
    val DEFAULT_DETAILS_STRING = DAY_MONTH_YEAR_DETAILS_STRING

}