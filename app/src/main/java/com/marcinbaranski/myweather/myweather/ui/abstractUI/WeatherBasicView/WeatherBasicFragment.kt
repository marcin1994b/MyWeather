package com.marcinbaranski.myweather.myweather.ui.abstractUI.WeatherBasicView

import android.widget.Toast
import com.marcinbaranski.myweather.myweather.model.providers.PicassoProvider
import com.marcinbaranski.myweather.myweather.ui.abstractUI.BasicFragment
import kotlinx.android.synthetic.main.today_weather_layout.*


abstract class WeatherBasicFragment: BasicFragment(), WeatherBasicViewContracts.WeatherBasicViewContract {

    override fun showToast(msg: String) = Toast.makeText(this.context, msg, Toast.LENGTH_SHORT).show()

    override fun setWeatherTextView(str: String) {
        weatherDetailTextView.text = str
    }

    override fun setTemperatureTextView(str: String) {
        temperatureDetailTextView.text = str
    }

    override fun setHumidityTextView(str: String) {
        humidityDetailTextView.text = str
    }

    override fun setPressureTextView(str: String) {
        pressureDetailTextView.text = str
    }

    override fun setWindSpeedTextView(str: String) {
        windDetailTextView.text = str
    }

    override fun setSunRiseTextView(str: String) {
        sunRiseDetailTextView.text = str
    }

    override fun setSunSetTextView(str: String) {
        sunSetDetailTextView.text = str
    }

    override fun setCloudCoverTextView(str: String) {
        cloudCoverDetailTextView.text = str
    }

    override fun setMoonPhaseTextView(str: String) {
        moonDetailTextView.text = str
    }

    override fun setImageView(link: String) {
        val picassoProvider = PicassoProvider(this.context)
        picassoProvider.setImage(link, weatherDetailImageView)
    }
}