package com.marcinbaranski.myweather.myweather.ui.ForecastView

import com.marcinbaranski.myweather.myweather.model.holders.WeatherResponseHolder


class ForecastPresenter(private val view: ForecastContracts.ForecastFragmentContracts)
    : ForecastContracts.ForecastPresenterContracts, WeatherResponseHolder.ObserverContract  {

    override fun onViewCreated() {
        setListView()
    }

    override fun onResume(){
        WeatherResponseHolder.addSubscriber(this)
    }

    override fun onStop() {
        WeatherResponseHolder.removeSubscriber(this)
    }

    private fun setListView() = view.initForecastList(WeatherResponseHolder.weatherResponseData.daily)

    override fun update() = view.refreshRecyclerItems()

    override fun onStateChange(newValue: Boolean) {}

    override fun onItemClick(position: Int) = view.startDetailActivity(position)

}