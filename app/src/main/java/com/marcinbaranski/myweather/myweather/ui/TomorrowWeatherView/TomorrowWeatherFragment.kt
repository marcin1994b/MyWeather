package com.marcinbaranski.myweather.myweather.ui.TomorrowWeatherView

import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcinbaranski.myweather.myweather.R
import com.marcinbaranski.myweather.myweather.ui.MainView.MainActivity
import com.marcinbaranski.myweather.myweather.ui.abstractUI.WeatherBasicView.WeatherBasicFragment
import kotlinx.android.synthetic.main.today_weather_layout.*

class TomorrowWeatherFragment : WeatherBasicFragment(), TomorrowWeatherContracts.TomorrowWeatherViewContract {

    private lateinit var presenter: TomorrowWeatherContracts.TomorrowWeatherPresenterContract

    companion object {
        fun newInstance(): Fragment = TomorrowWeatherFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater?.inflate(R.layout.today_weather_layout, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        initPresenter()
        swipeRefreshLayout.setOnRefreshListener { presenter.onRefreshListener() }
        //presenter.onViewCreated()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun askMainActivityAboutDownloadData() {
        (activity as MainActivity).presenter.refreshWeatherDataForFragment()
    }

    override fun hideRefreshingBar() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun showRefreshingBar() {
        swipeRefreshLayout.isRefreshing = true
    }

    private fun setupView(){
        firstTitleTextView.text = getString(R.string.details)
        secondTitleTextView.visibility = View.GONE
        dividerView1.visibility = View.GONE
        tempRangeLableTextView.visibility = View.GONE

        val constraintSet = ConstraintSet()
        constraintSet.clone(constraintTmp)
        constraintSet.connect(textView14.id, ConstraintSet.TOP, textView6.id, ConstraintSet.BOTTOM, 20)
        constraintSet.connect(sunRiseDetailTextView.id, ConstraintSet.TOP, windDetailTextView.id, ConstraintSet.BOTTOM, 20)
        constraintSet.setGuidelinePercent(guideline1.id, 0.405f)
        constraintSet.setGuidelinePercent(guideline2.id, 0.48f)
        constraintSet.applyTo(constraintTmp)
    }

    private fun initPresenter(){
        presenter = TomorrowWeatherPresenter(this)
    }
}