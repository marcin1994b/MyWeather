package com.marcinbaranski.myweather.myweather.model.entity.weatherData

import com.marcinbaranski.myweather.myweather.model.entity.imageData.ImageData

data class WeatherResponse(
        var cityName: String = "",
        var latitude: Double = 0.0,
        var longitude: Double = 0.0,
        var timezone: String = "",
        var units: String = "",
        var unitsType: Int = 0,
        var currently: WeatherData = WeatherData(),
        var daily: DailyWeatherData = DailyWeatherData(),
        var todayWeatherImg: ImageData = ImageData("",""),
        var tomorrowWeatherImg: ImageData = ImageData("", "")
)
