package com.marcinbaranski.myweather.myweather.ui.abstractUI.DownloadingBasicActivity

import com.google.android.gms.location.places.Place
import com.marcinbaranski.myweather.myweather.model.constants.UnitsTypes
import com.marcinbaranski.myweather.myweather.model.holders.SettingsHolder
import com.marcinbaranski.myweather.myweather.model.providers.NetworkProvider
import com.marcinbaranski.myweather.myweather.model.providers.PermissionsProvider
import com.marcinbaranski.myweather.myweather.model.providers.SharedPreferencesProvider
import com.marcinbaranski.myweather.myweather.model.providers.TimeDataProvider
import com.marcinbaranski.myweather.myweather.model.services.DataDownloadingServices


abstract class DownloadingBasicViewPresenter(private val permissionsProvider: PermissionsProvider,
                                    private val networkProvider: NetworkProvider,
                                    private val sharedPreferencesProvider: SharedPreferencesProvider,
                                    private val dataDownloadingServices: DataDownloadingServices):
    PermissionsProvider.onPermissionsProviderResult, DataDownloadingServices.onDataDownloadingServiceResult {

    override fun onPermissionsGranted() =
            when(networkProvider.isNetworkAvailable()){
                true -> dataDownloadingServices.startDownloading(getPlace(), getWeatherUnitsTypeString(),
                        SettingsHolder.weatherUnitsType)
                false-> noInternetConnection()
            }

    abstract fun getPlace(): Place?
    abstract fun noInternetConnection()

    //override fun onResume() = dataDownloadingServices.onResume()

    //override fun onPause() = dataDownloadingServices.onPause()

    //override fun onStop() = dataDownloadingServices.onStop()

    fun downloadData(){
        setSettings()
        saveDownloadingTimeStamp()
        dataDownloadingServices.receiver = this
        checkPermissions()
    }

    private fun setSettings(){
        SettingsHolder.weatherUnitsType = sharedPreferencesProvider.restoreWeatherUnits()
        SettingsHolder.timeFormatType = sharedPreferencesProvider.restoreTimeFormatType()
        SettingsHolder.dateFormatType = sharedPreferencesProvider.restoreDateFormatType()
    }

    private fun saveDownloadingTimeStamp(){
        val timeProvider = TimeDataProvider()
        sharedPreferencesProvider.saveLastDataUpdateTimeStamp(timeProvider.dt.millis)
    }

    private fun checkPermissions() {
        permissionsProvider.receiver = this
        permissionsProvider.checkPermissions()
    }

    private fun getWeatherUnitsTypeString(): String =
            when(SettingsHolder.weatherUnitsType){
                UnitsTypes.CA -> UnitsTypes.CA_STRING
                UnitsTypes.SI -> UnitsTypes.SI_STRING
                else -> UnitsTypes.US_STRING
            }
}