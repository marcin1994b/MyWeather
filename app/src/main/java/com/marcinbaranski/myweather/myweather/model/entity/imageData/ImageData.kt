package com.marcinbaranski.myweather.myweather.model.entity.imageData

data class ImageData(var id: String, var link: String)
