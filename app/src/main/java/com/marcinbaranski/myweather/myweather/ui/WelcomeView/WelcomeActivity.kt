package com.marcinbaranski.myweather.myweather.ui.WelcomeView

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import com.marcinbaranski.myweather.myweather.R
import com.marcinbaranski.myweather.myweather.model.providers.LocationProvider
import com.marcinbaranski.myweather.myweather.model.providers.NetworkProvider
import com.marcinbaranski.myweather.myweather.model.providers.PermissionsProvider
import com.marcinbaranski.myweather.myweather.model.providers.SharedPreferencesProvider
import com.marcinbaranski.myweather.myweather.model.services.DataDownloadingServices
import com.marcinbaranski.myweather.myweather.ui.MainView.MainActivity
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.choose_units_layout.view.*
import kotlinx.android.synthetic.main.error_layout.view.*
import kotlinx.android.synthetic.main.welcome_layout.*


class WelcomeActivity: AppCompatActivity(), WelcomeViewContracts.WelcomeViewContract {

    private lateinit var presenter: WelcomeViewContracts.WelcomeViewPresenterContract

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.welcome_layout)
        Fabric.with(this, Crashlytics())
        initPresenter()
        presenter.onViewCreate()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun showToast(msg: String) =
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()

    override fun setChooseUnitsLayout() {
        with(chooseUnitsInclude){
            this.visibility = View.VISIBLE
            this.findViewById<Button>(R.id.goButton).setOnClickListener {
                presenter.onDownloadWeatherDataButtonTap(
                        this.radio_first.isChecked,
                        this.radio_second.isChecked,
                        this.radio_third.isChecked
                )
            }
        }

    }

    override fun setNoInternetConnectionLayout() =
            setErrorLayout(getString(R.string.no_internet_connection),
                    getDrawable(R.drawable.ic_no_internet_connection))

    override fun setNoLocationLayout() =
            setErrorLayout(getString(R.string.no_location),
                    getDrawable(R.drawable.ic_no_location))

    override fun setPermissionsDeniedLayout() =
            setErrorLayout(getString(R.string.no_permissions),
                    getDrawable(R.drawable.ic_no_permissions))

    override fun setDownloadingDataLayout() {
        downloadingInclude.visibility = View.VISIBLE
    }

    override fun setSomethingWentWrongLayout() =
            setErrorLayout(getString(R.string.ups_something_went_wrong),
                    getDrawable(R.drawable.ic_no_permissions))

    override fun hideChooseUnitsLayout() {
        chooseUnitsInclude.visibility = View.INVISIBLE
    }

    override fun hideErrorLayout() {
        errorInclude.visibility = View.INVISIBLE
    }

    override fun hideDownloadingLayout() {
        downloadingInclude.visibility = View.INVISIBLE
    }

    override fun startMainView() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        this.finish()
    }


    private fun setErrorLayout(str: String, icon: Drawable){
        with(errorInclude){
            this.visibility = View.VISIBLE
            this.iconImageView.background = icon
            this.errorMsgTextView.text = str
            this.retryButton.setOnClickListener {
                presenter.onRetryButtonTap()
            }
        }
    }

    private fun initPresenter(){
        presenter = WelcomeViewPresenter(this, NetworkProvider(this),
                SharedPreferencesProvider(this), PermissionsProvider(this),
                DataDownloadingServices(LocationProvider(this), NetworkProvider(this)))
    }
}