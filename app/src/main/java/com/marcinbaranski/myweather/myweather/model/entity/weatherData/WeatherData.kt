package com.marcinbaranski.myweather.myweather.model.entity.weatherData

data class WeatherData(
        var time: Long = 0,
        var summary: String = "",
        var icon: String = "",
        var temperature: Double = 0.0,
        var temperatureMin: Double = 0.0,
        var temperatureMax: Double = 0.0,
        var humidity: Double = 0.0,
        var pressure: Double = 0.0,
        var windSpeed: Double = 0.0,
        var cloudCover: Double = 0.0,
        var sunriseTime: Long = 0,
        var sunsetTime: Long = 0,
        var moonPhase: Double = 0.0
)