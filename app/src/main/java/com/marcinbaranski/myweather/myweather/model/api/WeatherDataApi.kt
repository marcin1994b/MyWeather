package com.marcinbaranski.myweather.myweather.model.api

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.marcinbaranski.myweather.myweather.model.entity.weatherData.WeatherResponse
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

class WeatherDataApi {

    private interface weatherDataProvider{
//
        @GET("forecast/{api_key}/{latitude},{longitude}/")
        fun getWeatherData(@Path("api_key") api_key: String,
                           @Path("latitude") latitude: Double,
                           @Path("longitude") longitude: Double,
                           @Query("units") units: String) : Single<WeatherResponse>

    }

    private object APIClient {

        val BASIC_URL : String = "https://api.darksky.net/"
        var retrofit : Retrofit

        init {
            retrofit = Retrofit.Builder()
                    .baseUrl(BASIC_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
        }
    }

    private val API_KEY : String = "3b7840ec8584fd54dc6039ea0e999a50"
    private val apiService : weatherDataProvider = APIClient.retrofit.create(weatherDataProvider::class.java)

    fun getWeatherData(latitude: Double, longitude: Double, units: String): Single<WeatherResponse> =
            apiService.getWeatherData(API_KEY, latitude, longitude, units)


}