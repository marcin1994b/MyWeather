package com.marcinbaranski.myweather.myweather.ui.abstractUI

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View


abstract class BasicFragment: Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }


}