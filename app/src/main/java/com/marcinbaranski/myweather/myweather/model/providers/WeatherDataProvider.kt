package com.marcinbaranski.myweather.myweather.model.providers

import com.marcinbaranski.myweather.myweather.model.api.WeatherDataApi
import com.marcinbaranski.myweather.myweather.model.entity.weatherData.WeatherResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


// 15 + 10  17 + 10(I) =
class WeatherDataProvider(private val receiver: WeatherDataProvider) {

    interface WeatherDataProvider{
        fun handleWeatherDataProviderResponse(response: WeatherResponse)
        fun handleWeatherDataProviderError(error: Throwable)
    }

    fun downloadForecastData(latitude: Double, longitude: Double, units: String){
        val api = WeatherDataApi()
        api.getWeatherData(latitude, longitude, units)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleWeatherResponse, this::handleWeatherError)
    }

    private fun handleWeatherResponse(weatherResponse: WeatherResponse) =
            receiver.handleWeatherDataProviderResponse(weatherResponse)

    private fun handleWeatherError(error: Throwable) =
            receiver.handleWeatherDataProviderError(error)
}