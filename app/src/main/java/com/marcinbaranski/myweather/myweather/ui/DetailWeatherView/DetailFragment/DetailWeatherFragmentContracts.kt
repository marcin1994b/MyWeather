package com.marcinbaranski.myweather.myweather.ui.DetailWeatherView.DetailFragment

/**
 * Created by Marcin on 18.05.2018.
 */
interface DetailWeatherFragmentContracts {

    interface View{

        fun setWeatherDescriptionTextView(str: String)
        fun setTemperatureTextView(str: String)
        fun setHumidityTextView(str: String)
        fun setPressureTextView(str: String)
        fun setWindTextView(str: String)
        fun setSunriseTextView(str: String)
        fun setSunsetTextView(str: String)
        fun setCloudCoverTextView(str: String)
        fun setMoonPhaseTextView(str: String)
        fun setImageView(link: String)
    }

    interface Presenter{
        fun onResume(position: Int)
    }
}