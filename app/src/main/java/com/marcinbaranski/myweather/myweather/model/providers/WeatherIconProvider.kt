package com.marcinbaranski.myweather.myweather.model.providers

import android.content.Context
import com.marcinbaranski.myweather.myweather.R
import com.marcinbaranski.myweather.myweather.model.constants.WeatherTypes

class WeatherIconProvider(private val context: Context) {

    fun getWeatherIcon(icon: String): String =
        when(icon){
            WeatherTypes.CLEAR_DAY -> context.getString(R.string.wi_day_sunny)
            WeatherTypes.CLEAR_NIGHT -> context.getString(R.string.wi_day_sunny)
            WeatherTypes.RAIN -> context.getString(R.string.wi_day_rain)
            WeatherTypes.SNOW -> context.getString(R.string.wi_day_snow)
            WeatherTypes.SLEET -> context.getString(R.string.wi_day_sleet)
            WeatherTypes.WIND -> context.getString(R.string.wi_day_windy)
            WeatherTypes.FOG -> context.getString(R.string.wi_day_fog)
            WeatherTypes.PARTLY_CLOUDY_DAY -> context.getString(R.string.wi_day_sunny_overcast)
            WeatherTypes.PARTLY_CLOUDY_NIGHT -> context.getString(R.string.wi_day_sunny_overcast)
            WeatherTypes.HAIL -> context.getString(R.string.wi_day_hail)
            WeatherTypes.THUNDERSTORM -> context.getString(R.string.wi_day_thunderstorm)
            WeatherTypes.TORNADO -> context.getString(R.string.wi_tornado)
            WeatherTypes.CLOUDY -> context.getString(R.string.wi_day_cloudy)
            else -> context.getString(R.string.wi_na)
        }
}