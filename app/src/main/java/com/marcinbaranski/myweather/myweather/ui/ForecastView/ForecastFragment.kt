package com.marcinbaranski.myweather.myweather.ui.ForecastView

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.marcinbaranski.myweather.myweather.R
import com.marcinbaranski.myweather.myweather.model.adapters.ForecastListAdapter
import com.marcinbaranski.myweather.myweather.model.entity.weatherData.DailyWeatherData
import com.marcinbaranski.myweather.myweather.ui.DetailWeatherView.DetailWeatherActivity
import com.marcinbaranski.myweather.myweather.ui.abstractUI.BasicFragment

class ForecastFragment : BasicFragment(), ForecastContracts.ForecastFragmentContracts{

    var myRecyclerView: RecyclerView? = null
    lateinit var presenter: ForecastContracts.ForecastPresenterContracts

    companion object {
        fun newInstance(): Fragment = ForecastFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater?.inflate(R.layout.forecast_layout, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPresenter()
        presenter.onViewCreated()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun showToast(msg: String) =
            Toast.makeText(this.context, msg, Toast.LENGTH_SHORT).show()


    private fun initPresenter() {
        presenter = ForecastPresenter(this)
    }

    override fun initForecastList(weatherData: DailyWeatherData){
        myRecyclerView = view?.findViewById(R.id.my_recyclerview)
        myRecyclerView?.layoutManager = LinearLayoutManager(this.context)
        myRecyclerView?.adapter = ForecastListAdapter(weatherData, this.context, this::onItemClickListener)

    }

    override fun startDetailActivity(position: Int) {
        val intent = Intent(this.context, DetailWeatherActivity::class.java)
        intent.putExtra("position", position)
        startActivity(intent)
    }

    private fun onItemClickListener(position: Int){
        presenter.onItemClick(position)
    }

    override fun refreshRecyclerItems() {
        myRecyclerView?.adapter?.notifyDataSetChanged()
    }


}