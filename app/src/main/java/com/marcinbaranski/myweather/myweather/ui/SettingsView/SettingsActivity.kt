package com.marcinbaranski.myweather.myweather.ui.SettingsView

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.Switch
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.marcinbaranski.myweather.myweather.R
import com.marcinbaranski.myweather.myweather.model.providers.SharedPreferencesProvider
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.settings_layout.*
import kotlinx.android.synthetic.main.settings_view_item.view.*

class SettingsActivity: AppCompatActivity(), SettingsViewContracts.SettingsView {

    private lateinit var presenter: SettingsViewContracts.SettingsViewPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_layout)
        Fabric.with(this, Crashlytics())
        initPresenter()
        setToolbar()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        presenter.onViewCreate()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean =
            when(item?.itemId){
                android.R.id.home -> {
                    presenter.onHomeMenuButtonTap()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    override fun finishActivity() = this.finish()

    override fun setUnitsTypeItem(currentUnitsType: String) {
        with(unitsTypeItem){
            this.firstTextView.text = "Units type"
            this.secondTextView.text = currentUnitsType
            this.imageView.background = getDrawable(R.drawable.ic_settings2)
            this.switchView.visibility = View.INVISIBLE
            this.setOnClickListener {
                presenter.onUnitsTypeItemTap()
            }
        }
    }

    override fun setDateFormatItem(currentDateFormat: String) {
        with(dateFormatItem){
            this.firstTextView.text = "Date format"
            this.secondTextView.text = currentDateFormat
            this.imageView.background = getDrawable(R.drawable.ic_date)
            this.switchView.visibility = View.INVISIBLE
            this.setOnClickListener {
                presenter.onDateFormatItemTap()}
        }
    }

    override fun setSecondTextViewInTimeFormatItem(str: String) {
        timeFormatItem.secondTextView.text = str
    }

    override fun setTimeFormatItem(isSwitchChecked: Boolean) {
        with(timeFormatItem){
            this.firstTextView.text = context.getString(R.string.use_24_hours_format)
            when(this.switchView.isActivated){
                true -> this.secondTextView.text = context.getString(R.string.example_24hours_format)
                false-> this.secondTextView.text = context.getString(R.string.example_12hours_format)
            }
            this.switchView.visibility = View.VISIBLE
            this.imageView.background = getDrawable(R.drawable.ic_time)
            this.setOnClickListener {
                //presenter.onTimeFormatItemTap(this.switchView.isActivated)
            }
            this.switchView.isChecked = isSwitchChecked
            this.switchView.setOnClickListener {
                presenter.onTimeFormatSwitchTap((it as Switch).isChecked)
            }
        }
    }

    override fun showUnitsTypeDialog(itemsList: Array<String>, checkedItem: Int) {
        val dialog = AlertDialog.Builder(this)
                .setTitle(getString(R.string.units_type_dialog_title))
                .setSingleChoiceItems(itemsList, checkedItem,
                        { _, i -> presenter.onUnitsTypeDialogItemChecked(i)}
                )
                .setPositiveButton(getString(R.string.submit_button),
                        { _, _ -> presenter.onUnitsTypeDialogSubmit() }
                )
                .setNegativeButton(getString(R.string.cancel_button),
                        { _, _ -> }
                )
                .create()
        dialog.show()
    }

    override fun showDateFormatDialog(itemsList: Array<String>, checkedItem: Int) {
        val dialog = AlertDialog.Builder(this)
                .setTitle(getString(R.string.date_format_dialog_title))
                .setSingleChoiceItems(itemsList, checkedItem,
                        { _, i -> presenter.onDateFormatDialogItemChecked(i)}
                )
                .setPositiveButton(getString(R.string.submit_button),
                        { _, _ -> presenter.onDateFormatDialogSubmit() }
                )
                .setNegativeButton(getString(R.string.cancel_button),
                        { _, _ -> }
                )
                .create()
        dialog.show()
    }

    override fun fabricLogDateFormatChange(format: String) =
            Answers.getInstance().logCustom(CustomEvent("DateFormatChange")
                    .putCustomAttribute("Format", format)
            )

    override fun fabricLogHourFormatChange(format: String) =
            Answers.getInstance().logCustom(CustomEvent("HourFormatChange")
                    .putCustomAttribute("Format", format)
            )

    override fun fabricLogUnitsFormatChange(format: String) =
            Answers.getInstance().logCustom(CustomEvent("UnitFormatChange")
                    .putCustomAttribute("Format", format)
            )

    override fun showToast(msg: String) =
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()

    private fun initPresenter(){
        presenter = SettingsViewPresenter(this, SharedPreferencesProvider(this))
    }

    private fun setToolbar(){
        my_toolbar.title = "Settings"
        setSupportActionBar(my_toolbar)
    }
}