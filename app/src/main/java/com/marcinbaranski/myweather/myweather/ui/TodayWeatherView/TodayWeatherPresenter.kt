package com.marcinbaranski.myweather.myweather.ui.TodayWeatherView

import com.marcinbaranski.myweather.myweather.model.builder.WeatherStringBuilder
import com.marcinbaranski.myweather.myweather.model.entity.weatherData.WeatherData
import com.marcinbaranski.myweather.myweather.model.entity.weatherData.WeatherResponse
import com.marcinbaranski.myweather.myweather.model.holders.WeatherResponseHolder
import com.marcinbaranski.myweather.myweather.ui.abstractUI.WeatherBasicView.WeatherBasicFragmentPresenter

class TodayWeatherPresenter(private val view: TodayWeatherContracts.TodayWeatherViewContract)
    : WeatherBasicFragmentPresenter(view), TodayWeatherContracts.TodayWeatherPresenterContract,
        WeatherResponseHolder.ObserverContract{

    override fun onViewCreated() {}

    override fun onResume() {
        WeatherResponseHolder.addSubscriber(this)
        when(WeatherResponseHolder.isDownloading){
            true -> {
                view.showRefreshingBar()
            }
            false-> setTextViewsIfItIsPossible()
        }
    }

    override fun onStop() {
        WeatherResponseHolder.removeSubscriber(this)
    }

    override fun setTextViews(){
        with(WeatherResponseHolder.weatherResponseData){
            val stringBuilder = WeatherStringBuilder(WeatherResponseHolder.weatherResponseData.unitsType)
            setWeatherTextView(this.currently, stringBuilder)
            setTemperatureTextView(this.currently, stringBuilder)
            setHumidityTextView(this.currently, stringBuilder)
            setPressureTextView(this.currently, stringBuilder)
            setWindSpeedTextView(this.currently, stringBuilder)
            setTemperatureRangeTextView(this, stringBuilder)
            setCloudCoverTextView(this.currently, stringBuilder)
            setMoonPhaseTextView(this.daily.data[0], stringBuilder)
            setSunRiseTextView(this.daily.data[0], stringBuilder)
            setSunSetTextView(this.daily.data[0], stringBuilder)
            setImageView(WeatherResponseHolder.weatherResponseData.todayWeatherImg)
            this.currently
        }
    }

    override fun setTemperatureTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setTemperatureTextView(stringBuilder.getTemperatureString(data.temperature))

    private fun setTemperatureRangeTextView(data: WeatherResponse, stringBuilder: WeatherStringBuilder) =
            view.setTemperatureRangeTextView(stringBuilder.getTemperatureRangeString(
                    data.daily.data[0].temperatureMin, data.daily.data[0].temperatureMax))

}