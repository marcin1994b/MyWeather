package com.marcinbaranski.myweather.myweather.ui.abstractUI.WeatherBasicView

interface WeatherBasicViewContracts {

    interface WeatherBasicViewContract {
        fun setWeatherTextView(str: String)
        fun setTemperatureTextView(str: String)
        fun setHumidityTextView(str: String)
        fun setPressureTextView(str: String)
        fun setWindSpeedTextView(str: String)
        fun setSunRiseTextView(str: String)
        fun setSunSetTextView(str: String)
        fun setCloudCoverTextView(str: String)
        fun setMoonPhaseTextView(str: String)
        fun setImageView(link: String)
        fun showRefreshingBar()
        fun hideRefreshingBar()
        fun showToast(msg: String)
        fun askMainActivityAboutDownloadData()
    }

    interface WeatherBasicViewPresenter{
        fun onViewCreated()
        fun onRefreshListener()
        fun onStop()
        fun onResume()
    }
}