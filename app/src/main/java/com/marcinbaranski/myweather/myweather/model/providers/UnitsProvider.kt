package com.marcinbaranski.myweather.myweather.model.providers

import com.marcinbaranski.myweather.myweather.model.constants.UnitsTypes

class UnitsStringProvider(private val units: Int) {

    private val celsius = "°C"
    private val fahrenheit = "°F"

    private val meterPerSecond = "m/s"
    private val kilometerPerHour = "km/h"
    private val milePerHour = "mph"

    private val millibar = "mb"
    private val hectopascal = "hPa"

    // us: farenheit, mile per hour, millibar
    // si: celsius, meter per second, hectopascal
    // ca: celsius, kilometer per hour, hectopascal

    fun getTemperatureUnitString(): String =
            when(units){
                UnitsTypes.US -> fahrenheit
                else -> celsius
            }

    fun getWindSpeedUnitString(): String =
            when(units){
                UnitsTypes.SI -> meterPerSecond
                UnitsTypes.CA -> kilometerPerHour
                else -> milePerHour
            }

    fun getPressureUnitString(): String =
            when(units){
                UnitsTypes.US -> millibar
                else -> hectopascal
            }

}