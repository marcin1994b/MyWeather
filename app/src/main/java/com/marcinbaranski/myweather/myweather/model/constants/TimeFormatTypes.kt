package com.marcinbaranski.myweather.myweather.model.constants


object TimeFormatTypes {

    val US = 1
    val EU = 2
    val DEFAULT = EU
}