package com.marcinbaranski.myweather.myweather.ui.ForecastView

import com.marcinbaranski.myweather.myweather.model.entity.weatherData.DailyWeatherData

interface ForecastContracts {

    interface ForecastFragmentContracts{
        fun initForecastList(weatherData: DailyWeatherData)
        fun refreshRecyclerItems()
        fun showToast(msg: String)
        fun startDetailActivity(position: Int)
    }

    interface ForecastPresenterContracts{
        fun onViewCreated()
        fun onStop()
        fun onResume()
        fun onItemClick(position: Int)
    }
}