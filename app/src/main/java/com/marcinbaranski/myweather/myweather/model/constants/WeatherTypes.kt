package com.marcinbaranski.myweather.myweather.model.constants

object WeatherTypes {
    val CLEAR_DAY = "clear-day"
    val CLEAR_NIGHT = "clear-night"
    val RAIN = "rain"
    val SNOW = "snow"
    val SLEET = "sleet"
    val WIND = "wind"
    val FOG = "fog"
    val PARTLY_CLOUDY_DAY = "partly-cloudy-day"
    val PARTLY_CLOUDY_NIGHT = "partly-cloudy-night"
    val HAIL = "hail"
    val THUNDERSTORM = "thunderstorm"
    val TORNADO = "tornado"
    val CLOUDY = "cloudy"
}