package com.marcinbaranski.myweather.myweather.model.repo

class WeatherImagesRepo {

    val CLEAR_DAY = 0
    val RAIN = 1
    val SNOW = 2
    val SLEET = 3
    val WIND = 4
    val FOG = 5
    val PARTLY_CLOUDY = 6
    val THUNDERSTORM = 7
    val CLOUDY = 8
    val HAIL = RAIN

    val list = listOf<List<String>>(
            //clear day
        listOf("7n1g7EnGhQI", "LXGKvnff7SQ", "K5u3jBp-VKA", "3QHXkAGIvP4", "M09dOdDJ6Rk",
                "fg0OV4nbx3E", "-IZhwQpd7To", "bp8Nw_Q11gI", "FxU8KV7psMY", "QtYMfcbdl8k",
                "9NUeLk0uqME", "t1mqA3V3-7g", "rbUjTpP3ylc", "dZ5rtJGwdIQ", "ig-lw0Dtz34"),
            //rain
        listOf("HNx4QLRgy2k", "VR0s3Yqm2RA", "4raYy7dY2yw", "F-t5EpfQNpk", "_87lZuOyg64",
                "n3iyDLjUGEI", "5t4isI9DNzc", "YCVSPCz72Rk", "6B9y2VdJy_k", "w7wA3YQgxUQ",
                "F3vbFTQ2v-o", "JgDUVGAXsso", "Bu1zj2WbjHE", "kUvslz9OCGo", "cm1gRsbjRlE"),
            //snow
        listOf("3by7JGEnGxc", "s-NDdg8SOCc", "J_mM1SsfkdY", "BDHtdul2bnI", "rtMiBkMCOsw",
                "2OSEWkQHiGI", "4vwv13RXqsQ", "TD8CbG9-sMk", "xFunHeSh3kU", "zYJkisrhskE",
                "ucrtTAfwJTk", "JVtcrWcbj1c", "d3pTF3r_hwY", "de0P588zgls", "QLfjlyEamV4"),
            //sleet
        listOf("SH4GNXNj1RA", "dXWRATKqz2k", "p7LpXu2lnwI", "5s1pIwDi8eA", "UF8qWEcbzZ0",
                "0QhXAI5bFVM", "4vwv13RXqsQ", "6mUkjz-LqRo", "2FCo3xmECgg", "oZMgy8s1Mh8"),
            //wind
        listOf("Ioy3OB0U5cI", "VQqt3g0BixA", "9Ng6mvu7ZNg", "ItGkpXAr8gk", "0x4-xx8NhcY",
                "MJ5rrwk1UVE", "ifpIUxbtu7M", "Kyd6YIhtfBU", "PRumW--tkc4", "pF_2lrjWiJE"),
            //fog
        listOf("-pCHz5XiMb0", "Pimi_9akwIs", "TFyi0QOx08c", "EuuIa8XTLK4", "S9543y3UEgg",
                "qUToqliACNA", "GSXHEh-5IMI", "pV69pG9YeEw", "cv_fqvrbKWs", "xx5DqzlenYk",
                "vxVZzpk6PHM", "3ytjETpQMNY", "OHMg0Hgetn4", "nIMpbMMe5y4", "EPmvy-Ql4U0"),
            //partly-cloudy
        listOf("BdQk6Qm3vAU", "kmAAlcld6wA", "8NSpHrD1FLw", "_MNnowJVClg", "BSuY8m8VxRU",
                "3LsocYqXWpM", "e7fyg1_yAvQ", "88PQxSVFvwE", "QY5IX6_Btgo", "FFLGD4o149E",
                "SaPWcxZvbHA", "LXx9SS18jhI", "cbEa_YtwcQA", "LPfFjlSkl9E", "u2-97eBhFEc"),
            //hail -> rain
            //thunderstorm
        listOf("trnTvywx2Rg", "5Mh8iz9vqpY", "Cm5zI68Wdew", "NcTQ602gKLI", "E-Zuyev2XWo",
                "C8J0dFvxDqk", "___Lnn9HGko", "AJFdEo-DKCg", "vP5Im4q8Z6g", "NVMF-cAHxCg",
                "NROJUYgpjKE", "uxwKpUvwGuY", "LHdST1-f2bc", "GmBT6GQEOs0", "LYq7W1lRal4"),
            //cloudy
        listOf("sqKJxvLue3Q", "Pjduhf7li54", "J0VPtx1TPZ4", "AAHL8DtvBcg", "il_S6Vvtuus",
                "jJe9bx5uh-8", "-GwHS2piM_U", "Y1ByvAGQ5iE", "UTQUVi6WlEA", "shpZfAPb4no",
                "wW0FtXDjyIQ", "ifpIUxbtu7M", "mK-LBRSG1rA", "qQIb6343A8U", "qpgHO-oBkXQ")
    )
}