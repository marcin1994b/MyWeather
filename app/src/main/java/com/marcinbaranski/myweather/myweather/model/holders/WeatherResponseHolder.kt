package com.marcinbaranski.myweather.myweather.model.holders

import android.util.Log
import com.marcinbaranski.myweather.myweather.model.entity.weatherData.WeatherResponse
import kotlin.properties.Delegates

object WeatherResponseHolder {

    interface ObserverContract{
        fun update()
        fun onStateChange(newValue: Boolean)
    }

    private var subscribers: ArrayList<ObserverContract> = arrayListOf()

    var weatherResponseData: WeatherResponse by Delegates.observable(WeatherResponse()){
        _, _, _ ->
        Log.i("WETHER DATA HOLDER", "Weather data changed")
        for (item in subscribers) item.update()

    }

    var isDownloading: Boolean by Delegates.observable(false){
        _, old, newValue ->
        println("Zmieniono z $old na $newValue")
        for( item in subscribers) item.onStateChange(newValue)
    }

    fun addSubscriber(subscriber: ObserverContract) = subscribers.add(subscriber)

    fun removeSubscriber(subscriber: ObserverContract) = subscribers.remove(subscriber)

}