package com.marcinbaranski.myweather.myweather.ui.WelcomeView


interface WelcomeViewContracts {

    interface WelcomeViewContract{
        fun showToast(msg: String)
        fun setChooseUnitsLayout()
        fun setNoInternetConnectionLayout()
        fun setNoLocationLayout()
        fun setPermissionsDeniedLayout()
        fun setSomethingWentWrongLayout()
        fun setDownloadingDataLayout()
        fun hideChooseUnitsLayout()
        fun hideErrorLayout()
        fun hideDownloadingLayout()
        fun startMainView()
    }

    interface WelcomeViewPresenterContract{
        fun onViewCreate()
        fun onRetryButtonTap()
        fun onDownloadWeatherDataButtonTap(first: Boolean, seconde: Boolean, third: Boolean)
    }
}