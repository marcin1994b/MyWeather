package com.marcinbaranski.myweather.myweather.ui

interface FragmentContract {

    fun refreshWeatherDataForFragment()
}