package com.marcinbaranski.myweather.myweather.ui

interface ObserverContract {

    fun update()
}