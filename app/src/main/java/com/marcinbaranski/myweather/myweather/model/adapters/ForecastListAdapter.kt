package com.marcinbaranski.myweather.myweather.model.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcinbaranski.myweather.myweather.R
import com.marcinbaranski.myweather.myweather.model.builder.WeatherStringBuilder
import com.marcinbaranski.myweather.myweather.model.entity.weatherData.DailyWeatherData
import com.marcinbaranski.myweather.myweather.model.entity.weatherData.WeatherData
import com.marcinbaranski.myweather.myweather.model.holders.WeatherResponseHolder
import com.marcinbaranski.myweather.myweather.model.providers.TimeDataProvider
import com.marcinbaranski.myweather.myweather.model.providers.WeatherIconProvider
import kotlinx.android.synthetic.main.forecast_item.view.*

class ForecastListAdapter(val dailyWeatherData: DailyWeatherData, val context: Context,
                          private val onItemClick: (Int)->Unit)
    : RecyclerView.Adapter<ForecastListAdapter.ViewHolder>(){

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bindData(dailyWeatherData.data[position], position)
    }

    override fun getItemCount(): Int = dailyWeatherData.data.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.forecast_item,
                parent, false)
        return ViewHolder(view, context, onItemClick)

    }

    class ViewHolder(val view: View, val context: Context, private val onItemClick: (Int) -> Unit)
        : RecyclerView.ViewHolder(view){

        fun bindData(weatherData: WeatherData, position: Int){
            setTextData(weatherData, position)
            setImageData(weatherData)
            itemView.setOnClickListener { onItemClick(position) }
        }

        private fun setTextData(weatherData: WeatherData, position:Int){
            val weatherStringBuilder = WeatherStringBuilder(WeatherResponseHolder.weatherResponseData.unitsType)
            val timeDataProvider = TimeDataProvider()
            timeDataProvider.addDays(position)
            with(weatherData) {
                itemView.dateTextView.text = weatherStringBuilder.getDateString(
                        timeDataProvider.getDayOfMonth(), timeDataProvider.getMonthOfYear(),
                        timeDataProvider.getYear())
                itemView.temperatureDetailTextView.text = weatherStringBuilder.getTemperatureRangeString(
                        temperatureMin, temperatureMax)
                itemView.pressureDetailTextView.text = weatherStringBuilder.getPressureString(pressure)
                itemView.windDetailTextView.text = weatherStringBuilder.getWindSpeedString(windSpeed)
            }
        }

        private fun setImageData(weatherData: WeatherData){
            val weatherIconProvider = WeatherIconProvider(context)
            itemView.weatherDetailImageView.setIconResource(weatherIconProvider.getWeatherIcon(weatherData.icon))
        }

    }
}