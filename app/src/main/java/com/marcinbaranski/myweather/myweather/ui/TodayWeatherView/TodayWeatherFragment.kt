package com.marcinbaranski.myweather.myweather.ui.TodayWeatherView

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcinbaranski.myweather.myweather.R
import com.marcinbaranski.myweather.myweather.ui.MainView.MainActivity
import com.marcinbaranski.myweather.myweather.ui.abstractUI.WeatherBasicView.WeatherBasicFragment
import kotlinx.android.synthetic.main.today_weather_layout.*

class TodayWeatherFragment : WeatherBasicFragment(), TodayWeatherContracts.TodayWeatherViewContract {

    private lateinit var presenter: TodayWeatherContracts.TodayWeatherPresenterContract

    companion object {
        private var instance: TodayWeatherFragment = TodayWeatherFragment()
        fun newInstance(): Fragment = instance
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater?.inflate(R.layout.today_weather_layout, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPresenter()
        swipeRefreshLayout.setOnRefreshListener { presenter.onRefreshListener() }
        //presenter.onViewCreated()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }


    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun askMainActivityAboutDownloadData() {
        (activity as MainActivity).presenter.refreshWeatherDataForFragment()
    }

    override fun hideRefreshingBar() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun showRefreshingBar() {
        swipeRefreshLayout.isRefreshing = true
    }

    private fun initPresenter(){
        presenter = TodayWeatherPresenter(this)
    }

    override fun setTemperatureRangeTextView(str: String) {
        tempRangeTextView.text = str
    }
}