package com.marcinbaranski.myweather.myweather.ui.TodayWeatherView

import com.marcinbaranski.myweather.myweather.ui.abstractUI.WeatherBasicView.WeatherBasicViewContracts


interface TodayWeatherContracts {

    interface TodayWeatherViewContract: WeatherBasicViewContracts.WeatherBasicViewContract{
        fun setTemperatureRangeTextView(str: String)
    }

    interface TodayWeatherPresenterContract : WeatherBasicViewContracts.WeatherBasicViewPresenter

}