package com.marcinbaranski.myweather.myweather.model.constants


object UnitsTypes {

    val SI = 0
    val CA = 1
    val US = 2
    val DEFAULT = SI

    val SI_STRING = "si"
    val CA_STRING = "ca"
    val US_STRING = "us"
    val DEFAULT_STRING = SI_STRING

    val SI_DETAILS_STRING = "°C, m/s, hPa"
    val CA_DETAILS_STRING = "°C, km/h, hPa"
    val US_DETAILS_STRING = "°F, mph, mb"
    val DEFAULT_DETAILS_STRING = SI_DETAILS_STRING

}