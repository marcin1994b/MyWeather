package com.marcinbaranski.myweather.myweather.model.holders

object SettingsHolder {

    var weatherUnitsType: Int = 0
    var timeFormatType: Int = 0
    var dateFormatType: Int = 0
}