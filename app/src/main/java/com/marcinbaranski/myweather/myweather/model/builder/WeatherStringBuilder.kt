package com.marcinbaranski.myweather.myweather.model.builder

import com.marcinbaranski.myweather.myweather.model.constants.TimeFormatTypes
import com.marcinbaranski.myweather.myweather.model.constants.WeatherTypes
import com.marcinbaranski.myweather.myweather.model.providers.UnitsStringProvider

class WeatherStringBuilder(units: Int) {

    private val percent = "%"

    private val unitsProvider = UnitsStringProvider(units)
    private var stringBuilder = StringBuilder()

    fun getWeatherDescribeString(weatherType: String): String =
            when(weatherType){
                WeatherTypes.CLEAR_DAY -> "Clear"
                WeatherTypes.CLEAR_NIGHT -> "Clear"
                WeatherTypes.RAIN -> "Rain"
                WeatherTypes.SNOW -> "Snow"
                WeatherTypes.SLEET -> "Sleet"
                WeatherTypes.WIND -> "Wind"
                WeatherTypes.FOG -> "Fog"
                WeatherTypes.PARTLY_CLOUDY_DAY -> "Partly cloudy"
                WeatherTypes.PARTLY_CLOUDY_NIGHT -> "Partly cloudy"
                WeatherTypes.HAIL -> "Hail"
                WeatherTypes.THUNDERSTORM -> "Thunderstorm"
                WeatherTypes.TORNADO -> "Tornado"
                WeatherTypes.CLOUDY -> "Cloudy"
                else -> ""
            }

    fun getTemperatureString(temp: Double): String {
        stringBuilder.setLength(0)
        return stringBuilder
                .append(temp)
                .append(unitsProvider.getTemperatureUnitString())
                .toString()
    }

    fun getTemperatureRangeString(minTemp: Double, maxTemp: Double): String {
        stringBuilder.setLength(0)
        return stringBuilder
                .append(minTemp.toInt())
                .append(unitsProvider.getTemperatureUnitString())
                .append(" - ")
                .append(maxTemp.toInt())
                .append(unitsProvider.getTemperatureUnitString())
                .toString()
    }

    fun getPressureString(pressure: Double): String{
        stringBuilder.setLength(0)
        return stringBuilder
                .append(pressure.toInt())
                .append(" ")
                .append(unitsProvider.getPressureUnitString())
                .toString()
    }


    fun getWindSpeedString(windSpeed: Double): String{
        stringBuilder.setLength(0)
        return stringBuilder
                .append(windSpeed)
                .append(" ")
                .append(unitsProvider.getWindSpeedUnitString())
                .toString()
    }

    fun getHumidityString(humidity: Double): String{
        stringBuilder.setLength(0)
        return stringBuilder
                .append((humidity*100).toInt())
                .append(percent)
                .toString()
    }

    fun getHourAndMinutesString(hour: Int, minute: Int, timeFormatType: Int): String =
            when(timeFormatType){
                TimeFormatTypes.US -> build12FormatTimeString(hour, minute)
                else -> build24FormatTimeString(hour, minute)
            }

    private fun build12FormatTimeString(hour: Int, minute: Int): String{
        stringBuilder.setLength(0)
        return stringBuilder
                .append(get12HourFormatTimeHour(hour))
                .append(":")
                .append(getTwoDigitsNumberString(minute))
                .append(" ")
                .append(getPMorAMString(hour))
                .toString()
    }

    private fun build24FormatTimeString(hour: Int, minute: Int): String{
        stringBuilder.setLength(0)
        return stringBuilder
                .append(getTwoDigitsNumberString(hour))
                .append(":")
                .append(getTwoDigitsNumberString(minute))
                .toString()
    }

    fun getTwoDigitsNumberString(number: Int): String{
        if(number < 10){ return "0"+number.toString() }
        return number.toString()
    }

    fun getCloudCoverString(cloudCover: Double): String{
        stringBuilder.setLength(0)
        return stringBuilder
                .append((cloudCover*100).toInt())
                .append(percent)
                .toString()
    }

    fun getMoonPhaseString(moonPhase: Double): String{
        return moonPhase.toString()
    }

    fun getDateString(day: Int, month: Int, year: Int): String{
        stringBuilder.setLength(0)
        return stringBuilder
                .append(day)
                .append(" ")
                .append(getMonthString(month))
                .append(" ")
                .append(year)
                .toString()
    }

    private fun getMonthString(month: Int): String =
            when(month){
                1 -> "January"
                2 -> "February"
                3 -> "March"
                4 -> "April"
                5 -> "May"
                6 -> "June"
                7 -> "July"
                8 -> "August"
                9 -> "September"
                10-> "October"
                11-> "November"
                12-> "December"
                else-> ""
            }

    private fun get12HourFormatTimeHour(hour: Int): String =
            when(hour){
                0 -> "12"
                12-> "12"
                else -> getTwoDigitsNumberString(hour%12)
            }

    private fun getPMorAMString(hour: Int): String =
            if (hour <= 11) {
                "AM"
            } else {
                "PM"
            }

}