package com.marcinbaranski.myweather.myweather.ui.MainView

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.crashlytics.android.answers.SearchEvent
import com.google.android.gms.location.places.AutocompleteFilter
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.kobakei.ratethisapp.RateThisApp
import com.marcinbaranski.myweather.myweather.R
import com.marcinbaranski.myweather.myweather.model.providers.LocationProvider
import com.marcinbaranski.myweather.myweather.model.providers.NetworkProvider
import com.marcinbaranski.myweather.myweather.model.providers.PermissionsProvider
import com.marcinbaranski.myweather.myweather.model.providers.SharedPreferencesProvider
import com.marcinbaranski.myweather.myweather.model.services.DataDownloadingServices
import com.marcinbaranski.myweather.myweather.ui.ForecastView.ForecastFragment
import com.marcinbaranski.myweather.myweather.ui.SettingsView.SettingsActivity
import com.marcinbaranski.myweather.myweather.ui.TodayWeatherView.TodayWeatherFragment
import com.marcinbaranski.myweather.myweather.ui.TomorrowWeatherView.TomorrowWeatherFragment
import com.marcinbaranski.myweather.myweather.ui.WelcomeView.WelcomeActivity
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_main.*




class MainActivity : AppCompatActivity(), MainActivityContracts.MainViewContract {

    private val PLACE_AUTOCOMPLETE_REQUEST_CODE = 1

    class MyPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

        override fun getCount(): Int = NUM_ITEMS

        override fun getItem(position: Int): Fragment? = when (position) {
            0 -> TodayWeatherFragment.newInstance()
            1 -> TomorrowWeatherFragment.newInstance()
            2 -> ForecastFragment.newInstance()
            else -> null
        }

        override fun getPageTitle(position: Int): CharSequence = when(position){
            0 -> "Today"
            1 -> "Tomorrow"
            2 -> "Forecast"
            else -> ""
        }

        companion object {
            private val NUM_ITEMS = 3
        }
    }

    var adapterViewPager: FragmentPagerAdapter? = null

    lateinit var presenter: MainActivityContracts.MainViewPresenterContract

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.activity_main)
        initViewPager()
        initToolbar()
        initPresenter()
        presenter.onViewCreate()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onRestart() {
        super.onRestart()
        presenter.onViewResume()
    }

    override fun restartActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        this.finish()
    }

    override fun startWelcomeActivity() {
        val intent = Intent(this, WelcomeActivity::class.java)
        startActivity(intent)
        this.finish()
    }

    override fun finishThisActivity() = this.finish()

    override fun showRateDialog() {
        if(intent.getStringExtra("caller") == null) {
            RateThisApp.onCreate(this)
            RateThisApp.init(RateThisApp.Config(3, 5))
            RateThisApp.setCallback(rateThisAppCallback())
            RateThisApp.showRateDialogIfNeeded(this)

        }
    }

    override fun showToast(msg: String) =
            Toast.makeText(this.applicationContext, msg, Toast.LENGTH_SHORT).show()

    override fun setToolbarString(str: String) {
        my_toolbar.title = str
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean =
            when(item?.itemId){
                R.id.search_menu_button -> {
                    presenter.onSearchMenuButtonTap()
                    true
                }
                R.id.settings_menu_button -> {
                    presenter.onSettingsMenuButtonTap()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    override fun startSearchActivity() {
        val typeFilter = AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build()
        val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                .setFilter(typeFilter)
                .build(this)
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
    }

    override fun startSettingsActivity() {
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) =
            when(requestCode){
                PLACE_AUTOCOMPLETE_REQUEST_CODE -> {
                    when(resultCode){
                        Activity.RESULT_OK -> presenter.onSearchActivityResultSuccess(
                                PlaceAutocomplete.getPlace(this, data))
                        Activity.RESULT_CANCELED -> presenter.onSearchActivityResultCanceled()
                        else -> presenter.onSearchActivityResultError(
                                PlaceAutocomplete.getStatus(this, data).statusMessage)
                    }
                }
                else -> {}
            }

    override fun fabricLogSearch(query: String) =
            Answers.getInstance().logSearch(SearchEvent()
                    .putQuery(query)
            )

    override fun fabricLogRatingDialogAction(action: String) =
            Answers.getInstance().logCustom(CustomEvent("Rating Dialog Action")
                    .putCustomAttribute("Action", action)
            )


    private fun initViewPager(){
        adapterViewPager = MyPagerAdapter(supportFragmentManager)
        vpPager.adapter = adapterViewPager
    }

    private fun initToolbar(){
        my_toolbar.title = ""
        setSupportActionBar(my_toolbar)
    }

    private fun initPresenter(){
        presenter = MainActivityPresenter(this, NetworkProvider(this),
                PermissionsProvider(this),  SharedPreferencesProvider(this.applicationContext),
                DataDownloadingServices(LocationProvider(this), NetworkProvider(this)))
    }

    private fun rateThisAppCallback() = object: RateThisApp.Callback{
        override fun onNoClicked() = presenter.onNoClickedRatingDialog()

        override fun onYesClicked() = presenter.onYesClickedRatingDialog()

        override fun onCancelClicked() = presenter.onCancelClickedRatingDialog()
    }

}
