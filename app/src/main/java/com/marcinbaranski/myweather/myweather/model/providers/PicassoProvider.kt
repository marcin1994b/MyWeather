package com.marcinbaranski.myweather.myweather.model.providers

import android.content.Context
import android.widget.ImageView
import com.squareup.picasso.Picasso

//picsso
class PicassoProvider(private val context: Context) {

    fun setImage(url: String, imgView: ImageView){
        Picasso.with(context)
                .load(url)
                .centerCrop()
                .fit()
                .into(imgView)

    }
}