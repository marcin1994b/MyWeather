package com.marcinbaranski.myweather.myweather.model.providers

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import com.marcinbaranski.myweather.myweather.model.constants.DateFormatTypes
import com.marcinbaranski.myweather.myweather.model.constants.TimeFormatTypes
import com.marcinbaranski.myweather.myweather.model.constants.UnitsTypes

class SharedPreferencesProvider(private val context: Context) {

    val PREFERENCES_NAME = "com.marcinbaranski.myweather"
    val LAST_DATA_UPDATE = "lastDataUpdate"
    val FIRST_START = "firstStart"
    val WEATHER_UNITS = "unitsType"
    val TIME_FORMAT = "timeFormat"
    val DATE_FORMAT = "dateFormat"

    val DEFAULT_START_TIME: Long = 0
    val DEFAULT_LAST_UPDATE_TIME: Long = -1

    private val preferences = context.getSharedPreferences(PREFERENCES_NAME, Activity.MODE_PRIVATE);

    @SuppressLint("ApplySharedPref")
    fun saveWeatherUnits(units: Int){
        val preferencesEditor = preferences.edit()
        preferencesEditor.putInt(WEATHER_UNITS, units)
        preferencesEditor.commit()
    }

    fun saveTimeFormat(format: Int){
        val preferencesEditor = preferences.edit()
        preferencesEditor.putInt(TIME_FORMAT, format)
        preferencesEditor.commit()
    }

    fun saveDateFormat(format: Int){
        val preferencesEditor = preferences.edit()
        preferencesEditor.putInt(DATE_FORMAT, format)
        preferencesEditor.commit()
    }

    @SuppressLint("ApplySharedPref")
    fun saveStartTimeStamp(timeStamp: Long){
        val preferencesEditor = preferences.edit()
        preferencesEditor.putLong(FIRST_START, timeStamp)
        preferencesEditor.commit()
    }

    @SuppressLint("ApplySharedPref")
    fun saveLastDataUpdateTimeStamp(timeStamp: Long){
        val preferencesEditor = preferences.edit()
        preferencesEditor.putLong(LAST_DATA_UPDATE, timeStamp)
        preferencesEditor.commit()
    }

    fun restoreWeatherUnits(): Int =
            preferences.getInt(WEATHER_UNITS, UnitsTypes.DEFAULT)

    fun restoreTimeFormatType(): Int =
            preferences.getInt(TIME_FORMAT, TimeFormatTypes.DEFAULT)

    fun restoreDateFormatType(): Int =
            preferences.getInt(DATE_FORMAT, DateFormatTypes.DEFAULT)

    fun restoreStartTimeStamp(): Long =
            preferences.getLong(FIRST_START, DEFAULT_START_TIME)

    fun restoreLastDataUpdateTimeStamp(): Long =
            preferences.getLong(LAST_DATA_UPDATE, DEFAULT_LAST_UPDATE_TIME)

}