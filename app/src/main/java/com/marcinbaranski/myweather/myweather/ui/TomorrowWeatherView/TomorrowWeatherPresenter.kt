package com.marcinbaranski.myweather.myweather.ui.TomorrowWeatherView

import com.marcinbaranski.myweather.myweather.model.builder.WeatherStringBuilder
import com.marcinbaranski.myweather.myweather.model.entity.weatherData.WeatherData
import com.marcinbaranski.myweather.myweather.model.holders.WeatherResponseHolder
import com.marcinbaranski.myweather.myweather.ui.abstractUI.WeatherBasicView.WeatherBasicFragmentPresenter


class TomorrowWeatherPresenter(private val view: TomorrowWeatherContracts.TomorrowWeatherViewContract)
    : WeatherBasicFragmentPresenter(view), TomorrowWeatherContracts.TomorrowWeatherPresenterContract,
    WeatherResponseHolder.ObserverContract{


    override fun onViewCreated() {}

    override fun onResume() {
        WeatherResponseHolder.addSubscriber(this)
        when(WeatherResponseHolder.isDownloading){
            true -> {
                view.showRefreshingBar()
            }
            false-> setTextViewsIfItIsPossible()
        }
    }

    override fun onStop() {
        WeatherResponseHolder.removeSubscriber(this)
    }

    override fun setTextViews(){
        with(WeatherResponseHolder.weatherResponseData.daily.data[1]){
            val stringBuilder = WeatherStringBuilder(WeatherResponseHolder.weatherResponseData.unitsType)
            setWeatherTextView(this, stringBuilder)
            setTemperatureTextView(this, stringBuilder)
            setHumidityTextView(this, stringBuilder)
            setPressureTextView(this, stringBuilder)
            setWindSpeedTextView(this, stringBuilder)
            setCloudCoverTextView(this, stringBuilder)
            setMoonPhaseTextView(this, stringBuilder)
            setSunRiseTextView(this, stringBuilder)
            setSunSetTextView(this, stringBuilder)
            setImageView(WeatherResponseHolder.weatherResponseData.tomorrowWeatherImg)
        }
    }

    override fun setTemperatureTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setTemperatureTextView(stringBuilder.getTemperatureRangeString(
                    data.temperatureMin, data.temperatureMax))
}