package com.marcinbaranski.myweather.myweather.ui.SettingsView

import com.marcinbaranski.myweather.myweather.model.constants.DateFormatTypes
import com.marcinbaranski.myweather.myweather.model.constants.TimeFormatTypes
import com.marcinbaranski.myweather.myweather.model.constants.UnitsTypes
import com.marcinbaranski.myweather.myweather.model.holders.SettingsHolder
import com.marcinbaranski.myweather.myweather.model.providers.SharedPreferencesProvider

class SettingsViewPresenter(private val view: SettingsViewContracts.SettingsView,
        private val sharedPreferencesProvider: SharedPreferencesProvider)
    : SettingsViewContracts.SettingsViewPresenter {

    private var unitsType = sharedPreferencesProvider.restoreWeatherUnits()
    private var dateFormatType = sharedPreferencesProvider.restoreDateFormatType()

    override fun onViewCreate() {
        view.setUnitsTypeItem(getUnitsString())
        view.setDateFormatItem(getDateString())
        view.setTimeFormatItem(is24HoursFormatActivated())
    }

    override fun onUnitsTypeItemTap() {
        val itemsList = arrayOf(UnitsTypes.SI_DETAILS_STRING, UnitsTypes.CA_DETAILS_STRING,
                UnitsTypes.US_DETAILS_STRING)
        view.showUnitsTypeDialog(itemsList, sharedPreferencesProvider.restoreWeatherUnits())
    }

    override fun onDateFormatItemTap() {
        val itemsList = arrayOf(DateFormatTypes.YEAR_MONTH_DAY_DETAILS_STRING,
                DateFormatTypes.DAY_MONTH_YEAR_DETAILS_STRING,
                DateFormatTypes.MONTH_DAY_YEAR_DETAILS_STRING)
        view.showDateFormatDialog(itemsList, sharedPreferencesProvider.restoreDateFormatType())
    }

    override fun onTimeFormatItemTap(isSwitched: Boolean) = onTimeFormatSwitchTap(isSwitched)

    override fun onTimeFormatSwitchTap(isSwitched: Boolean) {
        var format = TimeFormatTypes.DEFAULT
        when(isSwitched){
            true -> {
                format = TimeFormatTypes.EU
                view.setSecondTextViewInTimeFormatItem("13:00")
                view.showToast("24-hours format activated")
                view.fabricLogHourFormatChange("24-hours")
            }
            false -> {
                format = TimeFormatTypes.US
                view.setSecondTextViewInTimeFormatItem("01:00 PM")
                view.showToast("12-hours format activated")
                view.fabricLogHourFormatChange("12-hours")
            }
        }
        SettingsHolder.timeFormatType = format
        sharedPreferencesProvider.saveTimeFormat(format)
    }

    override fun onUnitsTypeDialogItemChecked(checkedItem: Int) {
        unitsType = when (checkedItem) {
            UnitsTypes.SI -> UnitsTypes.SI
            UnitsTypes.CA -> UnitsTypes.CA
            else -> UnitsTypes.US
        }
    }

    override fun onUnitsTypeDialogSubmit() {
        sharedPreferencesProvider.saveWeatherUnits(unitsType)
        SettingsHolder.weatherUnitsType = unitsType
        view.setUnitsTypeItem(getUnitsString())
        view.showToast("Units type successful saved.")
        view.fabricLogUnitsFormatChange(getUnitsString())
    }

    override fun onDateFormatDialogItemChecked(checkedItem: Int) {
        dateFormatType = when (checkedItem) {
            DateFormatTypes.YEAR_MONTH_DAY -> DateFormatTypes.YEAR_MONTH_DAY
            DateFormatTypes.DAY_MONTH_YEAR -> DateFormatTypes.DAY_MONTH_YEAR
            else -> DateFormatTypes.MONTH_DAY_YEAR
        }
    }

    override fun onDateFormatDialogSubmit() {
        sharedPreferencesProvider.saveDateFormat(dateFormatType)
        SettingsHolder.dateFormatType = dateFormatType
        view.setDateFormatItem(getDateString())
        view.showToast("Date format successful saved.")
        view.fabricLogDateFormatChange(getDateString())
    }

    override fun onHomeMenuButtonTap() = view.finishActivity()

    private fun getUnitsString(): String =
            when(SettingsHolder.weatherUnitsType){
                UnitsTypes.SI -> UnitsTypes.SI_DETAILS_STRING
                UnitsTypes.US -> UnitsTypes.US_DETAILS_STRING
                UnitsTypes.CA -> UnitsTypes.CA_DETAILS_STRING
                else -> UnitsTypes.DEFAULT_DETAILS_STRING
            }

    private fun getDateString(): String =
            when(SettingsHolder.dateFormatType){
                DateFormatTypes.DAY_MONTH_YEAR -> DateFormatTypes.DAY_MONTH_YEAR_DETAILS_STRING
                DateFormatTypes.MONTH_DAY_YEAR -> DateFormatTypes.MONTH_DAY_YEAR_DETAILS_STRING
                DateFormatTypes.YEAR_MONTH_DAY -> DateFormatTypes.YEAR_MONTH_DAY_DETAILS_STRING
                else -> DateFormatTypes.DEFAULT_DETAILS_STRING
            }

    private fun is24HoursFormatActivated(): Boolean =
            when(SettingsHolder.timeFormatType){
                TimeFormatTypes.US -> false
                else -> true
            }
}