package com.marcinbaranski.myweather.myweather.ui.WelcomeView

import com.google.android.gms.location.places.Place
import com.marcinbaranski.myweather.myweather.model.constants.UnitsTypes
import com.marcinbaranski.myweather.myweather.model.entity.weatherData.WeatherResponse
import com.marcinbaranski.myweather.myweather.model.holders.WeatherResponseHolder
import com.marcinbaranski.myweather.myweather.model.providers.NetworkProvider
import com.marcinbaranski.myweather.myweather.model.providers.PermissionsProvider
import com.marcinbaranski.myweather.myweather.model.providers.SharedPreferencesProvider
import com.marcinbaranski.myweather.myweather.model.providers.TimeDataProvider
import com.marcinbaranski.myweather.myweather.model.services.DataDownloadingServices
import com.marcinbaranski.myweather.myweather.ui.abstractUI.DownloadingBasicActivity.DownloadingBasicViewPresenter


class WelcomeViewPresenter(private val view: WelcomeViewContracts.WelcomeViewContract,
                           networkProvider: NetworkProvider,
                           private val sharedPreferencesProv: SharedPreferencesProvider,
                           permissionsProvider: PermissionsProvider,
                           dataDownloadingServices: DataDownloadingServices)
    : DownloadingBasicViewPresenter(permissionsProvider, networkProvider,
        sharedPreferencesProv, dataDownloadingServices), WelcomeViewContracts.WelcomeViewPresenterContract{


    override fun getPlace(): Place? = null

    //override fun getUnits(): String = getWeatherUnitsTypeString(sharedPreferencesProv.restoreWeatherUnits())

    override fun noInternetConnection() {
        view.hideDownloadingLayout()
        view.setNoInternetConnectionLayout()
    }

    override fun onPermissionsDenied() {
        view.hideDownloadingLayout()
        view.setPermissionsDeniedLayout()
    }

    override fun onDatDownloadingServiceResult(isSuccess: Boolean, data: WeatherResponse, error: Int) {
        when (isSuccess) {
            true -> {
                WeatherResponseHolder.weatherResponseData = data
                view.startMainView()
                view.hideDownloadingLayout()
            }
            false -> {
                view.hideDownloadingLayout()
                view.setSomethingWentWrongLayout()
            }
        }
    }

    override fun onViewCreate(){
        when(isFirstStart()){
            true -> view.setChooseUnitsLayout()
            false -> startDownloadingWeatherData()
        }
    }

    override fun onRetryButtonTap() = startDownloadingWeatherData()

    override fun onDownloadWeatherDataButtonTap(first: Boolean, second: Boolean, third: Boolean) {
        val timeProvider = TimeDataProvider()
        sharedPreferencesProv.saveWeatherUnits(getUnitName(first, second, third))
        sharedPreferencesProv.saveStartTimeStamp(timeProvider.dt.millis)
        startDownloadingWeatherData()
    }

    private fun startDownloadingWeatherData(){
        view.hideChooseUnitsLayout()
        view.hideErrorLayout()
        view.setDownloadingDataLayout()
        downloadData()
    }

    private fun isFirstStart(): Boolean =
            when(sharedPreferencesProv.restoreStartTimeStamp()){
                sharedPreferencesProv.DEFAULT_START_TIME -> true
                else -> false
            }

    private fun getUnitName(first: Boolean, second: Boolean, third: Boolean) =
            when(true){
                first -> UnitsTypes.US
                second -> UnitsTypes.SI
                third -> UnitsTypes.CA
                else -> UnitsTypes.DEFAULT
            }

    private fun getWeatherUnitsTypeString(type: Int): String =
            when(type){
                UnitsTypes.CA -> UnitsTypes.CA_STRING
                UnitsTypes.SI -> UnitsTypes.SI_STRING
                else -> UnitsTypes.US_STRING
            }

}