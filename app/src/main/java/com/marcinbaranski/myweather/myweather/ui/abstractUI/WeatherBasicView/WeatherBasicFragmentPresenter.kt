package com.marcinbaranski.myweather.myweather.ui.abstractUI.WeatherBasicView

import com.marcinbaranski.myweather.myweather.model.builder.WeatherStringBuilder
import com.marcinbaranski.myweather.myweather.model.entity.imageData.ImageData
import com.marcinbaranski.myweather.myweather.model.entity.weatherData.WeatherData
import com.marcinbaranski.myweather.myweather.model.holders.SettingsHolder
import com.marcinbaranski.myweather.myweather.model.holders.WeatherResponseHolder
import com.marcinbaranski.myweather.myweather.model.providers.TimeDataProvider

abstract class WeatherBasicFragmentPresenter(private val view: WeatherBasicViewContracts.WeatherBasicViewContract) :
        WeatherBasicViewContracts.WeatherBasicViewPresenter, WeatherResponseHolder.ObserverContract {

    override fun onViewCreated() = setTextViews()

    protected abstract fun setTextViews()

    protected abstract fun setTemperatureTextView(data: WeatherData, stringBuilder: WeatherStringBuilder)

    protected fun setWeatherTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setWeatherTextView(stringBuilder.getWeatherDescribeString(data.icon))

    protected fun setHumidityTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setHumidityTextView(stringBuilder.getHumidityString(data.humidity))

    protected fun setPressureTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setPressureTextView(stringBuilder.getPressureString(data.pressure))

    protected fun setWindSpeedTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setWindSpeedTextView(stringBuilder.getWindSpeedString(data.windSpeed))

    protected fun setCloudCoverTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setCloudCoverTextView(stringBuilder.getCloudCoverString(data.cloudCover))

    protected fun setMoonPhaseTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) =
            view.setMoonPhaseTextView(stringBuilder.getMoonPhaseString(data.moonPhase))

    protected fun setSunRiseTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) {
        val timeProvider = TimeDataProvider(data.sunriseTime*1000)
        view.setSunRiseTextView(stringBuilder.getHourAndMinutesString(timeProvider.getHourOfDay(),
                timeProvider.getMinuteOfHour(), SettingsHolder.timeFormatType))
    }

    protected fun setSunSetTextView(data: WeatherData, stringBuilder: WeatherStringBuilder) {
        val timeProvider = TimeDataProvider(data.sunsetTime*1000)
        view.setSunSetTextView(stringBuilder.getHourAndMinutesString(timeProvider.getHourOfDay(),
                timeProvider.getMinuteOfHour(), SettingsHolder.timeFormatType))
    }

    protected fun setImageView(data: ImageData) = view.setImageView(data.link)

    override fun onRefreshListener() {
        view.askMainActivityAboutDownloadData()
    }

    override fun update() = setTextViews()

    override fun onStateChange(newValue: Boolean) =
            when(newValue){
                true -> view.showRefreshingBar()
                false-> {
                    view.hideRefreshingBar()
                    setTextViewsIfItIsPossible()
                }
            }

    protected fun setTextViewsIfItIsPossible() =
            when(isWeatherDataResponseSet()) {
                true -> {
                    setTextViews()
                }
                false-> {}
            }

    private fun isWeatherDataResponseSet(): Boolean =
            WeatherResponseHolder.weatherResponseData.cityName != ""


}