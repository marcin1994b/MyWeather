package com.marcinbaranski.myweather.myweather.ui.DetailWeatherView


interface DetailWeatherContracts {

    interface View{
        fun startFragment(position: Int)
        fun finishTheActivity()
    }

    interface Presenter{
        fun onViewCreated(position: Int)
        fun onHomeButtonTap()
    }
}