package com.marcinbaranski.myweather.myweather.model.entity.weatherData

data class DailyWeatherData(
        var data: ArrayList<WeatherData> = ArrayList()
)