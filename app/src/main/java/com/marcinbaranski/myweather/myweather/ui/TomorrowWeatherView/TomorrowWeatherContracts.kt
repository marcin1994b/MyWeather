package com.marcinbaranski.myweather.myweather.ui.TomorrowWeatherView

import com.marcinbaranski.myweather.myweather.ui.abstractUI.WeatherBasicView.WeatherBasicViewContracts


interface TomorrowWeatherContracts {

    interface TomorrowWeatherViewContract: WeatherBasicViewContracts.WeatherBasicViewContract

    interface TomorrowWeatherPresenterContract: WeatherBasicViewContracts.WeatherBasicViewPresenter
}