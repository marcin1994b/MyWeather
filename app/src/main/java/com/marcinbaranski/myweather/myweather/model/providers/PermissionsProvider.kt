package com.marcinbaranski.myweather.myweather.model.providers

import android.Manifest
import android.app.Activity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener


class PermissionsProvider(private val activity: Activity) {

    var receiver: PermissionsProvider.onPermissionsProviderResult? = null

    interface onPermissionsProviderResult{
        fun onPermissionsGranted()
        fun onPermissionsDenied()
    }

    fun checkPermissions(){
        Dexter.withActivity(activity)
                .withPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?,
                                                                    token: PermissionToken?) {
                        token?.continuePermissionRequest()
                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        report?.let{
                            when(it.areAllPermissionsGranted()){
                                true -> receiver?.onPermissionsGranted()
                                false-> receiver?.onPermissionsDenied()
                            }
                        }
                    }
                }).check()

    }

}