package com.marcinbaranski.myweather.myweather.ui.DetailWeatherView

class DetailWeatherViewPresenter(private val view: DetailWeatherContracts.View) :
        DetailWeatherContracts.Presenter {

    override fun onViewCreated(position: Int) {
        when(position){
            -1 -> {}
            else -> view.startFragment(position)
        }
    }

    override fun onHomeButtonTap() = view.finishTheActivity()
}