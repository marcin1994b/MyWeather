package com.marcinbaranski.myweather.myweather.ui.SettingsView

interface SettingsViewContracts {

    interface SettingsView{
        fun setUnitsTypeItem(currentUnitsType: String)
        fun setDateFormatItem(currentDateFormat: String)
        fun setTimeFormatItem(isSwitchChecked: Boolean)
        fun showUnitsTypeDialog(itemsList: Array<String>, checkedItem: Int)
        fun showDateFormatDialog(itemsList: Array<String>, checkedItem: Int)
        fun showToast(msg: String)
        fun finishActivity()
        fun setSecondTextViewInTimeFormatItem(str: String)

        fun fabricLogUnitsFormatChange(format: String)
        fun fabricLogHourFormatChange(format: String)
        fun fabricLogDateFormatChange(format: String)
    }

    interface SettingsViewPresenter{
        fun onViewCreate()
        fun onUnitsTypeItemTap()
        fun onDateFormatItemTap()
        fun onUnitsTypeDialogItemChecked(checkedItem: Int)
        fun onUnitsTypeDialogSubmit()
        fun onDateFormatDialogItemChecked(checkedItem: Int)
        fun onDateFormatDialogSubmit()
        fun onTimeFormatItemTap(isSwitched: Boolean)
        fun onTimeFormatSwitchTap(isSwitched: Boolean)
        fun onHomeMenuButtonTap()
    }
}