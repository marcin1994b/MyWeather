package com.marcinbaranski.myweather.myweather.ui.DetailWeatherView.DetailFragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcinbaranski.myweather.myweather.R
import com.marcinbaranski.myweather.myweather.model.providers.PicassoProvider
import kotlinx.android.synthetic.main.weather_layout.*

class DetailWeatherFragment: Fragment(), DetailWeatherFragmentContracts.View {

    lateinit var presenter: DetailWeatherFragmentContracts.Presenter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater?.inflate(R.layout.weather_layout, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPresenter()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume(arguments.getInt("position", -1))
    }

    override fun setWeatherDescriptionTextView(str: String) {
        weatherDetailTextView.text = str
    }

    override fun setTemperatureTextView(str: String){
        temperatureDetailTextView.text = str
    }

    override fun setHumidityTextView(str: String){
        humidityDetailTextView.text = str
    }

    override fun setPressureTextView(str: String){
        pressureDetailTextView.text = str
    }

    override fun setWindTextView(str: String){
        windDetailTextView.text = str
    }

    override fun setSunriseTextView(str: String){
        sunRiseDetailTextView.text = str
    }

    override fun setSunsetTextView(str: String){
        sunSetDetailTextView.text = str
    }

    override fun setCloudCoverTextView(str: String){
        cloudCoverDetailTextView.text = str
    }

    override fun setMoonPhaseTextView(str: String){
        moonDetailTextView.text = str
    }

    override fun setImageView(link: String) {
        val picassoProvider = PicassoProvider(this.context)
        picassoProvider.setImage(link, weatherDetailImageView)
    }

    private fun initPresenter(){
        presenter = DetailWeatherFragmentPresenter(this)
    }

}