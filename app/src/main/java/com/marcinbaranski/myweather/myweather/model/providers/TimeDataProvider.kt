package com.marcinbaranski.myweather.myweather.model.providers

import org.joda.time.DateTime

class TimeDataProvider {

    var dt = DateTime()

    constructor(time: Long){
        dt = DateTime(time)
    }

    constructor(){
        dt = DateTime()
    }

    fun addDays(days: Int){
        dt = dt.plusDays(days)
    }

    fun getYear(): Int = dt.year

    fun getMonthOfYear(): Int = dt.monthOfYear

    fun getDayOfMonth(): Int = dt.dayOfMonth

    fun getHourOfDay(): Int = dt.hourOfDay

    fun getMinuteOfHour(): Int = dt.minuteOfHour

}